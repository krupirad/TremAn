# TremAn
TremaAn is an desktop application written in C# which measures frequency of tremor (e.g., Parkinsonian tremor) from videorecord.
## Main window
![TremAn main window](./docs/mainwindow.png)
## Chart window
![TremAn chart window](./docs/charts.png)
