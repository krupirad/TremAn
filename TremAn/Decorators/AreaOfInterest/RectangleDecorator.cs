﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Shapes;

namespace TremAn.Decorators.AreaOfInterest
{
    public class RectangleDecorator
    {
        private enum CursorType
        {
            None, Body, UL, UR, LR, LL, T, B, L, R
        };

        private Models.AreaOfInterest.Rectangle areaOfInterestRectangle;
        private Rectangle wpfRectangle;
        public delegate void MyCallback(Models.AreaOfInterest.Rectangle areaOfInterestRectangle);
        public MyCallback CallBack { get; set; }

        DependencyPropertyDescriptor topDescriptor = DependencyPropertyDescriptor.FromProperty(Canvas.TopProperty, typeof(Rectangle));
        DependencyPropertyDescriptor leftDescriptor = DependencyPropertyDescriptor.FromProperty(Canvas.LeftProperty, typeof(Rectangle));

        
        private CursorType mouseHitType = CursorType.None;
        private Point lastPoint;
        private bool dragInProgress = false;


        public RectangleDecorator(Models.AreaOfInterest.Rectangle areaOfInterestRectangle, Rectangle wpfRectangle)
        {
            this.areaOfInterestRectangle = areaOfInterestRectangle;
            this.wpfRectangle = wpfRectangle;


            wpfRectangle.MouseDown += OnMouseDown;
            wpfRectangle.MouseUp += OnMouseUp;
            wpfRectangle.MouseMove += OnMouseMove;
            topDescriptor.AddValueChanged(wpfRectangle, RectangleTopPositionChanged);
            leftDescriptor.AddValueChanged(wpfRectangle, RectangleLeftPositionChanged);
            wpfRectangle.SizeChanged += RectangleSizeChanged;
            SetRectangle(areaOfInterestRectangle);
        }

        public void SetRectangle(Models.AreaOfInterest.Rectangle areaOfInterestRectangle)
        {
            this.areaOfInterestRectangle = areaOfInterestRectangle;
            wpfRectangle.Width = areaOfInterestRectangle.Width;
            wpfRectangle.Height = areaOfInterestRectangle.Height;
            Canvas.SetLeft(wpfRectangle, areaOfInterestRectangle.X);
            Canvas.SetTop(wpfRectangle, areaOfInterestRectangle.Y);
        }

        private void RectangleLeftPositionChanged(object sender, EventArgs e) => areaOfInterestRectangle.X = Canvas.GetLeft(wpfRectangle);

        private void RectangleTopPositionChanged(object sender, EventArgs e) => areaOfInterestRectangle.Y = Canvas.GetTop(wpfRectangle);

        private void RectangleSizeChanged(object sender, EventArgs e)
        {
            areaOfInterestRectangle.Width = wpfRectangle.Width;
            areaOfInterestRectangle.Height = wpfRectangle.Height;
        }

        #region Rectangle Movement
        private void OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            wpfRectangle.CaptureMouse();
            dragInProgress = SetRectangleCursorAndGetMoving(e.GetPosition((Canvas)wpfRectangle.Parent));
        }

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            if (dragInProgress)
            {
                MoveRectangleInProgress(e.GetPosition((Canvas)wpfRectangle.Parent));
            }
            else
            {
                SetRectangleCursor(e.GetPosition((Canvas)wpfRectangle.Parent));
            }
        }

        private void OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            dragInProgress = false;
            wpfRectangle.ReleaseMouseCapture();
        }

        private CursorType SetHitType(Rectangle rectangle, System.Windows.Point point)
        {
            double left = Canvas.GetLeft(rectangle);
            double top = Canvas.GetTop(rectangle);
            double right = left + rectangle.Width;
            double bottom = top + rectangle.Height;
            if (point.X < left) return CursorType.None;
            if (point.X > right) return CursorType.None;
            if (point.Y < top) return CursorType.None;
            if (point.Y > bottom) return CursorType.None;

            const double GAP = 10;
            if (point.X - left < GAP)
            {
                if (point.Y - top < GAP) return CursorType.UL;
                if (bottom - point.Y < GAP) return CursorType.LL;
                return CursorType.L;
            }
            else if (right - point.X < GAP)
            {
                if (point.Y - top < GAP) return CursorType.UR;
                if (bottom - point.Y < GAP) return CursorType.LR;
                return CursorType.R;
            }
            if (point.Y - top < GAP) return CursorType.T;
            if (bottom - point.Y < GAP) return CursorType.B;
            return CursorType.Body;
        }

        private void SetMouseCursor()
        {
            Cursor desired_cursor = Cursors.Arrow;
            switch (mouseHitType)
            {
                case CursorType.None:
                    desired_cursor = Cursors.Arrow;
                    break;
                case CursorType.Body:
                    desired_cursor = Cursors.ScrollAll;
                    break;
                case CursorType.UL:
                case CursorType.LR:
                    desired_cursor = Cursors.SizeNWSE;
                    break;
                case CursorType.LL:
                case CursorType.UR:
                    desired_cursor = Cursors.SizeNESW;
                    break;
                case CursorType.T:
                case CursorType.B:
                    desired_cursor = Cursors.SizeNS;
                    break;
                case CursorType.L:
                case CursorType.R:
                    desired_cursor = Cursors.SizeWE;
                    break;
            }

            if (wpfRectangle.Cursor != desired_cursor) wpfRectangle.Cursor = desired_cursor;
        }

        private void MoveRectangleInProgress(Point point)
        {
            double new_x = Canvas.GetLeft(wpfRectangle);
            double new_y = Canvas.GetTop(wpfRectangle);
            double new_width = wpfRectangle.Width;
            double new_height = wpfRectangle.Height;

            switch (mouseHitType)
            {
                case CursorType.Body:
                    new_x += point.X - lastPoint.X;
                    new_y += point.Y - lastPoint.Y;
                    break;
                case CursorType.UL:
                    new_x += point.X - lastPoint.X;
                    new_y += point.Y - lastPoint.Y;
                    new_width -= point.X - lastPoint.X;
                    new_height -= point.Y - lastPoint.Y;
                    break;
                case CursorType.UR:
                    new_y += point.Y - lastPoint.Y;
                    new_width += point.X - lastPoint.X;
                    new_height -= point.Y - lastPoint.Y;
                    break;
                case CursorType.LR:
                    new_width += point.X - lastPoint.X;
                    new_height += point.Y - lastPoint.Y;
                    break;
                case CursorType.LL:
                    new_x += point.X - lastPoint.X;
                    new_width -= point.X - lastPoint.X;
                    new_height += point.Y - lastPoint.Y;
                    break;
                case CursorType.L:
                    new_x += point.X - lastPoint.X;
                    new_width -= point.X - lastPoint.X;
                    break;
                case CursorType.R:
                    new_width += point.X - lastPoint.X;
                    break;
                case CursorType.B:
                    new_height += point.Y - lastPoint.Y;
                    break;
                case CursorType.T:
                    new_y += point.Y - lastPoint.Y;
                    new_height -= point.Y - lastPoint.Y;
                    break;
            }

            if ((new_width > 0) && (new_height > 0))
            {
                wpfRectangle.Width = new_width;
                wpfRectangle.Height = new_height;
                Canvas.SetLeft(wpfRectangle, new_x);
                Canvas.SetTop(wpfRectangle, new_y);
                lastPoint = point;
            }
            CallBack?.Invoke(areaOfInterestRectangle);
        }

        private void SetRectangleCursor(Point mousePoint)
        {
            mouseHitType = SetHitType(wpfRectangle, mousePoint);
            SetMouseCursor();
        }

        private bool SetRectangleCursorAndGetMoving(Point mousePoint)
        {
            mouseHitType = SetHitType(wpfRectangle, mousePoint);
            SetMouseCursor();
            if (mouseHitType == CursorType.None) return false;
            else
            {
                lastPoint = mousePoint;
                return true;
            }
        }

        public Models.AreaOfInterest.Rectangle GetAreaOfInterestRectangle() => areaOfInterestRectangle;

        #endregion
        public void SaveAOIRectanglePosition() => CallBack?.Invoke(areaOfInterestRectangle);
    }
}