﻿using System.Linq;
using DotImaging;
using DotImaging.Primitives2D;
using System.Windows.Media.Imaging;

namespace TremAn
{
    class Drawer
    {
        public static BitmapSource CvToBitmapSource(Bgr<byte>[,] image)
        {
            return image.ToBitmapSource();
        }

        public static Gray<float>[,] ColorToGray(Bgr<byte>[,] image)
        {
            return image.ToGray().Cast<float>();
        }

        public static Bgr<byte>[,] GetAOIImage(Bgr<byte>[,] image, Models.AreaOfInterest.Rectangle areaOfInterestRectangle)
        {
            return image.Clone(new Rectangle((int)areaOfInterestRectangle.X, (int)areaOfInterestRectangle.Y, (int)areaOfInterestRectangle.Width, (int)areaOfInterestRectangle.Height));
        }

    }
}
