﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TremAn
{
    public static class Exporter
    {
        public static void ExportAll(String filename, double[][] intensities)
        {
            using (System.IO.StreamWriter f = new System.IO.StreamWriter(filename))
            {
                foreach (var i in intensities)
                {
                    f.WriteLine(String.Join(" ", i.Select(p => p.ToString(new System.Globalization.CultureInfo("en-US").NumberFormat)).ToArray()));
                }
            }
        }

        public static void ExportFrequencies(String filename, double[] frequencies)
        {
            using (System.IO.StreamWriter f = new System.IO.StreamWriter(filename))
            {
                f.WriteLine(String.Join("\n", frequencies.Select(p => p.ToString(new System.Globalization.CultureInfo("en-US").NumberFormat)).ToArray()));
            }
        }
    }
}
