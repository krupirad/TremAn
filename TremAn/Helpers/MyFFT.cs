﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using DSPLib;

namespace TremAn.Helpers
{
    static class MyFFT
    {
        public static double[] WelschFT(double[] inputSignal)
        {
            //double amplitude = 1.0; double frequency = 32768;
            //UInt32 length = 1024; double samplingRate = 131072;
            //double[] inputSignal = DSP.Generate.ToneSampling(amplitude, frequency, samplingRate, length);
            uint length = (uint) inputSignal.Length;
            // Apply window to the Input Data & calculate Scale Factor
            double[] wCoefs = DSP.Window.Coefficients(DSP.Window.Type.Welch, length);
            double[] wInputData = DSP.Math.Multiply(inputSignal, wCoefs);
            double wScaleFactor = DSP.Window.ScaleFactor.Signal(wCoefs);

            // Instantiate & Initialize a new DFT
            DSPLib.FFT fft = new DSPLib.FFT();
            fft.Initialize(length);

            // Call the FFT and get the scaled spectrum back
            Complex[] cSpectrum = fft.Execute(wInputData);

            // Convert the complex spectrum to note: Magnitude Squared Format
            // See text for the reasons to use Mag^2 format.
            return DSP.ConvertComplex.ToMagnitude(cSpectrum);

        }
    }
}
