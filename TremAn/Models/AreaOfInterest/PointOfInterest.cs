﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TremAn.Models.AreaOfInterest
{
    public class PointOfInterest
    {
        public int PositionX;
        public int PositionY;
        public int PositionIndex;
    }
}
