﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;


namespace TremAn.Models.AreaOfInterest
{
   
    public class Rectangle         
    {
        public double X;
        public double Y;
        public double Height;
        public double Width;
        public uint FrameID; 
    }
}
