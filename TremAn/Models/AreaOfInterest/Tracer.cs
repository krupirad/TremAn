﻿namespace TremAn.Models.AreaOfInterest
{
    public class Tracer
    {

        public Rectangle[] Rects{get;set;}

        public Tracer(uint numOfFrames, double defX = 0, double defY = 0, double defHeight = 100, double defWidth = 100)
        {
            Rects = new Rectangle[numOfFrames];
            for (uint i = 0; i < Rects.Length; i++)
            {
                Rects[i] = new Rectangle()
                {
                    X = defX,
                    Y = defY,
                    Width = defWidth,
                    Height = defHeight,
                    FrameID = i
                };

            }
        }

        public Tracer(Rectangle[] rects)
        {
            this.Rects = rects;
        }
    }
}