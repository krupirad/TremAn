﻿using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TremAn.Models
{
    public class Chart
    {
        public PlotModel FreqAnalysisChartModel { get; set; }
        public PlotModel[] FreqAnalysisChartModels { get; set; }
        public PlotModel FreqProgressChartModel { get; set; }
        public double[] FrequenciesToExport { get; set; }
        public double[][] SpectrumToExport { get; set; }
        public double CurrentFrequency { get; set; }
    }
}
