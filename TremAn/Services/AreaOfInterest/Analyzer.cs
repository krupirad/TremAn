﻿using System;
using System.Collections.Generic;
using System.Linq;
using DotImaging;
using DotImaging.Primitives2D;
using OxyPlot;
using OxyPlot.Series;
using OxyPlot.Axes;
using TremAn.Models.AreaOfInterest;
using TremAn.Helpers;
using System.Numerics;

namespace TremAn.Services.AreaOfInterest
{
    public class Analyzer
    {

        private PointOfInterest[] pointsGrid;
        private PlotModel frequencyAnalysisPlotModel;
        private Models.AreaOfInterest.Rectangle currentRectangle;
        private int gridStep = Properties.Settings.Default.GridStep;
        private float samplingFrequency;
        private double[] frequencies;
        private int lengthOfImageBuffer = 0;
        private PointOfInterest[] FoundP = null;
        private double[] resultedSpectrum = null;
        private double maxFrequency = 0;
        private int analyzationOffset = 0;

        #region Properties declaration

        public double[] ResultedSpectrum
        {
            get
            {
                return resultedSpectrum;
            }
        }

        public double MaxFrequency
        {
            get
            {
                return maxFrequency;
            }
        }

        public PlotModel FrequencyAnalysisPlotModel
        {
            get
            {
                return frequencyAnalysisPlotModel;
            }
        }
        #endregion

        public Analyzer(float samplingFrequency, int lengthOfImageBuffer)
        {
            this.samplingFrequency = samplingFrequency;
            this.lengthOfImageBuffer = lengthOfImageBuffer;
        }

        public void SetFrequencyArray(double[] frequencyArray)
        {
            this.frequencies = frequencyArray;
        }

        public void SetGrid(PointOfInterest[] pointsGrid)
        {
            this.pointsGrid = pointsGrid;
        }

        //Get all points of interest in area of interest
        private PointOfInterest[] FoundPointsOfInterest()
        {
            List<PointOfInterest> foundPointsOfInterest = new List<PointOfInterest>();
            for (int i = 0; i < pointsGrid.Count(); i++)
            {
                if (IsPointOfInterestInAreaOfInterest(pointsGrid[i])) foundPointsOfInterest.Add(pointsGrid[i]);
            }
            return foundPointsOfInterest.ToArray();
        }

        public void StartProcessing(Models.AreaOfInterest.Rectangle currentRectangle, List<Bgr<byte>[,]> frameBuffer, int analyzationOffset)
        {
            this.currentRectangle = currentRectangle;
            this.analyzationOffset = analyzationOffset;
            FoundP = FoundPointsOfInterest();
            resultedSpectrum = new double[lengthOfImageBuffer];
            for (int FoundPointIndex = 0; FoundPointIndex < FoundP.Count(); FoundPointIndex++)
            {
                GetPointOfInterestSignal(FoundP[FoundPointIndex], frameBuffer);
            }
            CropFullSpectrum();
            frequencyAnalysisPlotModel = CreatePlotModel(frequencies, resultedSpectrum, "Frequency Analysis");
            maxFrequency = GetMaxFrequency();
        }

        //Get signal for given point of interest
        private void GetPointOfInterestSignal(PointOfInterest pointOfInterest, List<Bgr<byte>[,]> frameBuffer)
        {
            double[] signalRed = new double[lengthOfImageBuffer];
            double[] signalGreen = new double[lengthOfImageBuffer];
            double[] signalBlue = new double[lengthOfImageBuffer];

            for (int frameNumberIndex = analyzationOffset; frameNumberIndex < lengthOfImageBuffer; frameNumberIndex++)
            {
                Bgr<byte> pixel = frameBuffer[frameNumberIndex][pointOfInterest.PositionY, pointOfInterest.PositionX];               
                signalRed[frameNumberIndex - analyzationOffset] = pixel.R;
                signalGreen[frameNumberIndex - analyzationOffset] = pixel.G;
                signalBlue[frameNumberIndex - analyzationOffset] = pixel.B;
            }
         


            FullSpectrum(MyFFT.WelschFT(signalRed), MyFFT.WelschFT(signalGreen), MyFFT.WelschFT(signalBlue));
        }

        //Transform color signals to frequency spectrum
        private void ForwardFFT(Complex[] signalRed, Complex[] signalGreen, Complex[] signalBlue)
        {

           
           // FourierTransform.FFT(signalRed, FourierTransform.Direction.Forward);
           // FourierTransform.FFT(signalGreen, FourierTransform.Direction.Forward);
           // FourierTransform.FFT(signalBlue, FourierTransform.Direction.Forward);
            FullSpectrum(signalRed, signalGreen, signalBlue);
        }

        //Generates vector of frequencies 
        public static double[] GenerateFrequencyVector(double lowestFrequency, int lengthOfImageBuffer, double samplingFrequency)
        {
            int lessThanHz = 0;
            double[] frequencies = new double[lengthOfImageBuffer / 2];
            for (int j = 0; j < lengthOfImageBuffer / 2; j++)
            {
                frequencies[j] = (samplingFrequency * j / lengthOfImageBuffer);
                if (frequencies[j] <= lowestFrequency) { lessThanHz++; };
            }
            return frequencies.Skip(lessThanHz).ToArray();
        }

        //Generate grid of points in area of interest
        public static PointOfInterest[] GenerateGrid(int width, int height, int step)
        {
            List<PointOfInterest> pointsGrid = new List<PointOfInterest>();
            for (int i = 0; i < width; i += step)
            {
                for (int j = 0; j < height; j += step)
                {
                    pointsGrid.Add(new PointOfInterest()
                    {
                        PositionX = i,
                        PositionY = j,
                    });
                }
            }
            return pointsGrid.ToArray();
        }

        //Get signals full frequency spectrum
        private void FullSpectrum(Complex[] signalRed, Complex[] signalGreen, Complex[] signalBlue)
        {
            for (int i = 0; i < lengthOfImageBuffer; i++)
            {
                resultedSpectrum[i] += SumSignals(signalRed[i], signalGreen[i], signalBlue[i]);
            }
        }
        private void FullSpectrum(double[] signalRed, double[] signalGreen, double[] signalBlue)
        {
            for (int i = 0; i < signalRed.Length; i++)
            {
                resultedSpectrum[i] += SumSignals(signalRed[i], signalGreen[i], signalBlue[i]);
            }
        }

        //Sum signals from color frequncy spectrums
        private double SumSignals(Complex pointRed, Complex pointGreen, Complex pointBlue)
        {
            double SpectrumRed = Math.Abs(pointRed.Real / lengthOfImageBuffer);
            double SpectrumGreen = Math.Abs(pointGreen.Real / lengthOfImageBuffer);
            double SpectrumBlue = Math.Abs(pointBlue.Real / lengthOfImageBuffer);
            return (SpectrumRed + SpectrumGreen + SpectrumBlue);
        }

        //Get highest frequency peak
        private double GetMaxFrequency()
        {
            int indexOfHighestPeak = Array.IndexOf(resultedSpectrum, (from number in resultedSpectrum
                                                                       orderby number descending
                                                                       select number).Distinct().First());

            return frequencies[indexOfHighestPeak];
        }

        private void CropFullSpectrum()
        {
            resultedSpectrum = resultedSpectrum.Take(lengthOfImageBuffer / 2 + 1).ToArray();
            int frequenciesLength = frequencies.Count();
            int spectrumLength = resultedSpectrum.Count();
            int skip = spectrumLength - frequenciesLength;
            resultedSpectrum = resultedSpectrum.Skip(skip).ToArray();
        }

        //Create model for plotting results
        private PlotModel CreatePlotModel(double[] frequencies, double[] fftResults, String title)
        {
            LineSeries series = new LineSeries();
            for (int i = 0; i < frequencies.Count(); i++)
            {
                series.Points.Add(new DataPoint(frequencies[i], fftResults[i]));
            }
            var frequencyAxis = new LinearAxis()
            {
                Position = AxisPosition.Bottom,
                Minimum = 2,
                Title = "Frequency(Hz)"
            };
            var intensityAxis = new LinearAxis()
            {
                Position = AxisPosition.Left,
                Minimum = 0
            };

            PlotModel model = new PlotModel { Title = title };
            
            model.PlotType = PlotType.XY;
            model.Series.Add(series);
            model.Axes.Add(frequencyAxis);
            model.Axes.Add(intensityAxis);
            return model;
        }

        private Complex GetComplexPixelChannel(byte channel)
        {
            return new Complex(channel, 0);
        }

        //Check if point of interest is in area of interest
        private bool IsPointOfInterestInAreaOfInterest(PointOfInterest pointOfInterest)
        {
            if (currentRectangle.X <= pointOfInterest.PositionX && pointOfInterest.PositionX <= currentRectangle.X + currentRectangle.Width)
            {
                if (currentRectangle.Y <= pointOfInterest.PositionY && pointOfInterest.PositionY <= currentRectangle.Y + currentRectangle.Height)
                {
                    return true;
                }
            }
            return false;
        }
    }
}