﻿using DotImaging;
using System.IO;

namespace TremAn.Services
{
    class Reader
    {
        private FileCapture fileCapture;
        private long startingPosition = 0;
        private long endingPosition = 0;

        public long StartingPosition
        {
            get { return startingPosition; }
            set
            {
                this.startingPosition = value;
            }
        }

        public long EndingPosition
        {
            get { return endingPosition; }
            set
            {
                this.endingPosition = value;
            }
        }

        public void Initialize(string filename)
        {
            fileCapture = new FileCapture(filename);
            this.endingPosition = fileCapture.Length - 20;
            fileCapture.Open();
        }

        public Bgr<byte>[,] NewFrame()
        {
            Bgr<byte>[,] buffer = null;
            fileCapture.ReadTo(ref buffer);
            return buffer;
        }

        public System.Windows.Media.Imaging.BitmapSource NewFrameInBitmapSource()
        {
            Bgr<byte>[,] buffer = null;
            fileCapture.ReadTo(ref buffer);
            return buffer.ToBitmapSource();
        }

        public float GetFrameRate()
        {
            if (fileCapture != null)
                return fileCapture.FrameRate;
            return 25;
        }

        public void CloseSource()
        {
            if (fileCapture == null)
                return;
            fileCapture.Dispose();
        }

        public int GetWidth()
        {
            if (fileCapture != null)
                return fileCapture.FrameSize.Width;
            else return 0;
        }

        public int GetHeight()
        {
            if (fileCapture != null)
                return fileCapture.FrameSize.Height;
            else return 0;
        }

        public void SeekToStart()
        {
            fileCapture.Seek(startingPosition, SeekOrigin.Begin);
        }

        public void GetFrameOnPosition(long position)
        {
            if (position <= endingPosition && position >= startingPosition) fileCapture.Seek(position, SeekOrigin.Begin);
        }

        public long GetFileCapturePosition()
        {
            return fileCapture.Position;
        }

        public long GetFileCaptureLength()
        {
            return endingPosition;
        }
    }
}
