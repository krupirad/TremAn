﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TremAn.Services
{
    public class ReaderStopWatch : Stopwatch
    {
        public TimeSpan StartOffset { get; set; }

        public ReaderStopWatch()
        {
            this.StartOffset = TimeSpan.FromMilliseconds(0);
        }

        public ReaderStopWatch(TimeSpan StartOffset)
        {
            this.StartOffset = StartOffset;
        }

        public new long ElapsedMilliseconds
        {
            get
            {
                return base.ElapsedMilliseconds + (long)StartOffset.TotalMilliseconds;
            }
        }

        public new long ElapsedTicks
        {
            get
            {
                return base.ElapsedTicks + StartOffset.Ticks;
            }
        }
    }
}
