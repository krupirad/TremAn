﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Xml.Linq;
using TremAn.Decorators.AreaOfInterest;
using TremAn.Models.AreaOfInterest;

namespace TremAn.ViewModels.AreaOfInterest
{
    public class TracerViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private Tracer Tracer;
        
        public Rectangle[] Rects
        {
            get
            {
                return Tracer.Rects;
            }
            set
            {
                Tracer.Rects = value;
                OnPropertyChanged("AOIRects");
            }
        }

        public TracerViewModel(Tracer tracer)
        {
            setTracer(tracer);
        }

        public void SetAOIRectCtrlCallback(RectangleDecorator rectangleDecorator)
        {
            rectangleDecorator.CallBack += RectangleChanged;
        }

        public void RectangleChanged(Rectangle rectangle)
        {
            SaveAOIPosition(rectangle);
        }

        private void setTracer(Tracer tracer)
        {
            this.Tracer = tracer;
        }

        //Export areas of interest to xml
        public XElement AreasOfInterestToXML()
        {
            XElement AreasOfInterestXML = new XElement("AreasOfInterest", from i in Rects
                                                                     select
                                                                       new XElement("AreaOfInterest",
                                                                       new XAttribute("Height", i.Height),
                                                                       new XAttribute("Width", i.Width),
                                                                       new XAttribute("X", i.X),
                                                                       new XAttribute("Y", i.Y),
                                                                       new XAttribute("frameID", i.FrameID)));
            return AreasOfInterestXML;
        }

        //Import areas of interest from xml
        internal static Tracer FromXML(XElement xe)
        {
            IEnumerable<Rectangle> rects = from rect in xe.Descendants("AreaOfInterest")
                                              select new Rectangle
                                              {
                                                  X = (double)rect.Attribute("X"),
                                                  Y = (double)rect.Attribute("Y"),
                                                  Width = (double)rect.Attribute("Width"),
                                                  Height = (double)rect.Attribute("Height"),
                                                  FrameID = UInt32.Parse(rect.Attribute("frameID").Value)
                                              };
            Tracer tracer = new Tracer(rects.ToArray());
            return tracer;
        }

        public Rectangle GetFrame(uint frameID)
        {
            return Rects[frameID];
        }

        public void SaveAOIPosition(Rectangle rectangle)
        {
            for (uint i = rectangle.FrameID; i < Rects.Length; i++)
            {
                Rects[i] = new Rectangle()
                {
                    X = (int)rectangle.X,
                    Y = (int)rectangle.Y,
                    Width = (int)rectangle.Width,
                    Height = (int)rectangle.Height,
                    FrameID = i
                };

            }
        }

        private void OnPropertyChanged(String info)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(info));
        }
    }
}
