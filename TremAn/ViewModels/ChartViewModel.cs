﻿using Microsoft.Win32;
using OxyPlot;
using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using TremAn.Models;

namespace TremAn.ViewModels
{
    public class ChartViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private Chart model;
        private ICommand exportCommand;
        private Visibility isVisible = Visibility.Visible;
        private bool isPrepared = true;
        private double majorFrequency = 0;
        private double averageFrequency = 0;

        #region Properties declaration for binding
        public Visibility IsVisible
        {
            get
            {
                return isVisible;
            }
            set
            {
                isVisible = value;
                OnPropertyChanged("IsVisible");
            }
        }

        public double AverageFrequency
        {
            get
            {
                return averageFrequency;
            }
            set
            {
                averageFrequency = value;
                OnPropertyChanged("AverageFrequency");
            }
        }

        public double MajorFrequency
        {
            get
            {
                return Math.Round(majorFrequency,2);
            }
            set
            {
                majorFrequency = value;
                OnPropertyChanged("MajorFrequency");
            }
        }

        public bool IsPrepared
        {
            get
            {
                return isPrepared;
            }
            set
            {
                isPrepared = value;
                OnPropertyChanged("IsPrepared");
            }
        }

        public ICommand ExportCommand
        {
            get
            {
                if (exportCommand == null)
                {
                    exportCommand = new Command(p => true, p => this.ExportData((string)p));    
                }
                return exportCommand;
            }
        }

        public double[][] SpectrumToExport
        {
            get
            {
                return model.SpectrumToExport;
            }
            set
            {
                model.SpectrumToExport = value;
            }
        }

        public double[] FrequenciesToExport
        {
            get
            {
                return model.FrequenciesToExport;
            }
            set
            {
                model.FrequenciesToExport = value;
                CountMajorFrequency();
                AverageFrequency = Math.Round(model.FrequenciesToExport.Average(), 2);
            }
        }

        public PlotModel[] FreqAnalysisChartModels
        {
            get
            {
                return model.FreqAnalysisChartModels;
            }
            set
            {
                model.FreqAnalysisChartModels = value;
            }
        }

        public PlotModel FreqAnalysisChartModel
        {
            get
            {
                return model.FreqAnalysisChartModel;
            }
            set
            {
                model.FreqAnalysisChartModel = value;
                OnPropertyChanged("FreqAnalysisChartModel");
            }
        }

        public PlotModel FreqProgressChartModel
        {
            get
            {
                return model.FreqProgressChartModel;
            }
            set
            {
                model.FreqProgressChartModel = value;
                IsVisible = Visibility.Hidden;
                OnPropertyChanged("FreqProgressChartModel");
            }
        }

        public double CurrentFrequency
        {
            get
            {
                return Math.Round(model.CurrentFrequency,2);
            }
            set
            {
                model.CurrentFrequency = value;
                OnPropertyChanged("CurrentFrequency");
            }
        }
        #endregion

        public ChartViewModel(Chart model)
        {
            this.model = model;
        }

        //Export data to file
        private void ExportData(string parameter)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text Files|*.txt|All Files|*.*";
            if (saveFileDialog.ShowDialog() == true)
            {
                string filename = saveFileDialog.FileName;
                switch (parameter)
                {
                    case "ALL": Exporter.ExportAll(filename,model.SpectrumToExport);
                        break;
                    case "FREQONLY": Exporter.ExportFrequencies(filename, model.FrequenciesToExport);
                        break;
                }
                
                return;
            }
            else return;        
        }

        //Finds and sets major frequency from resulted data
        private void CountMajorFrequency()
        {
            var frequent = from frequencies in model.FrequenciesToExport
                    group frequencies by frequencies into Groups
                    orderby Groups.Count()
                    select new { Number = Groups.Key, Freq = Groups.Count() };
            MajorFrequency = frequent.Last().Number;
        }

        private void OnPropertyChanged(String info)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(info));
        }
    }
}
