﻿using DotImaging;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using TremAn.Models;
using TremAn.Services;
using System.Xml.Linq;
using System.IO;
using System.Threading;
using OxyPlot;
using OxyPlot.Series;
using OxyPlot.Axes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using TremAn.Decorators.AreaOfInterest;
using System.Threading.Tasks;
using TremAn.Views;
using TremAn.Services.AreaOfInterest;
using TremAn.Models.AreaOfInterest;
using TremAn.ViewModels.AreaOfInterest;

namespace TremAn.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private Reader reader = null;
        private ICommand projectCommand;
        private ICommand videoControlCommand;
        private ICommand appControlCommand;
        private ICommand sliderCommand;
        private ICommand tracerCommand;
        private Tracer tracer = null;
        private String filename = null;
        private DispatcherTimer timerIdle = null;
        private ReaderStopWatch stopWatch = null;
        private BitmapSource bitSource;
        private ChartView chartView = null;
        private PropertiesView propertiesView = null;
        private ChartViewModel chartViewModel = null;
        private Analyzer[] analyzerPool = null;
        private bool isLoaded = false;
        private long fileCaptureLength = 20;
        private long croppedEnd = 0;
        private long croppedStart = 0;
        private long fileCapturePosition = 0;
        private double analyzationProgress = 0;
        private long timeElapsed = 0;
        private long offsetTimeElapsed = 0;
        private long timeElapsedInSeconds = 0;
        private long totalTimeInSeconds = 20;
        private long totalFramesToAnalyze = 20;
        private System.Windows.Shapes.Rectangle rectangleView;
        private int windowLength = 0;
        private string timeText = TimeSpan.FromMilliseconds(0).ToString(@"hh\:mm\:ss\:ff");
        private BitmapSource currentFrame;

        private static RectangleDecorator rectangleDecorator = null;
        private static TracerViewModel tracerViewModel = null;


        #region Properties declaration for binding

        public ICommand ProjectCommand
        {
            get
            {
                if (projectCommand == null)
                {
                    projectCommand = new Command(p => true, p => this.HandleProject((string)p));
                }
                return projectCommand;
            }
        }
        public ICommand TracerCommand
        {
            get
            {
                if (tracerCommand == null)
                {
                    tracerCommand = new Command(p => true, p => this.TracerSavePosition());
                }
                return tracerCommand;
            }
        }

        

        public ICommand SliderCommand
        {
            get
            {
                if (sliderCommand == null)
                {
                    sliderCommand = new Command(p => true, p => this.Slider_TargetUpdated((double)p));
                }
                return sliderCommand;
            }
        }
        public ICommand AppControlCommand
        {
            get
            {
                if (appControlCommand == null)
                {
                    appControlCommand = new Command(p => true, p => this.HandleApp((string)p));
                }
                return appControlCommand;
            }
        }



        public ICommand VideoControlCommand
        {
            get
            {
                if (videoControlCommand == null)
                {
                    videoControlCommand = new Command(p => true, p => this.HandleVideo((string)p));
                }
                return videoControlCommand;
            }
        }



        public long FileCaptureLength
        {
            get { return fileCaptureLength; }
            set
            {
                fileCaptureLength = value;
                OnPropertyChanged("FileCaptureLength");
            }
        }

        public long TotalFramesToAnalyze
        {
            get
            {
                return totalFramesToAnalyze;
            }
            set
            {
                totalFramesToAnalyze = value;
                OnPropertyChanged("TotalFramesToAnalyze");
            }
        }

        public bool IsProjectLoaded
        {
            get
            {
                return isLoaded;
            }
            set
            {
                isLoaded = value;
                OnPropertyChanged("IsProjectLoaded");
            }
        }

        public long CroppedStart
        {
            get { return croppedStart; }
            set
            {
                croppedStart = value;
                if (reader != null)
                {
                    reader.StartingPosition = croppedStart;
                }
                if (fileCapturePosition < croppedStart)
                {
                    FileCapturePosition = croppedStart;
                    Update();
                }
                OnPropertyChanged("CroppedStart");
            }
        }
        public long CroppedEnd
        {
            get { return croppedEnd; }
            set
            {
                croppedEnd = value;
                if (reader != null)
                {
                    reader.EndingPosition = croppedEnd;
                }
                if (fileCapturePosition > croppedEnd)
                {
                    FileCapturePosition = croppedEnd;
                    Update();
                }
                OnPropertyChanged("CroppedEnd");
            }
        }
        public double AnalyzationProgress
        {
            get { return analyzationProgress; }
            set
            {
                analyzationProgress = value;
                OnPropertyChanged("AnalyzationProgress");
            }
        }

        public long FileCapturePosition
        {
            get { return fileCapturePosition; }
            set
            {

                if (analyzerPool != null && analyzerPool[value] != null)
                {
                    chartViewModel.FreqAnalysisChartModel = analyzerPool[value].FrequencyAnalysisPlotModel;
                    chartViewModel.CurrentFrequency = analyzerPool[value].MaxFrequency;
                }
                fileCapturePosition = value;
                OnPropertyChanged("FileCapturePosition");
            }
        }
        public string TimeText
        {
            get
            {
                return timeText;
            }
            set
            {
                timeText = value;
                OnPropertyChanged("TimeText");
            }
        }

        public long TimeElapsedInSeconds
        {
            get
            {
                return timeElapsedInSeconds;
            }
            set
            {
                timeElapsedInSeconds = value;
                OnPropertyChanged("TimeElapsedInSeconds");
            }
        }

        public long TotalTimeInSeconds
        {
            get
            {
                return totalTimeInSeconds;
            }
            set
            {
                totalTimeInSeconds = value;
                OnPropertyChanged("TotalTimeInSeconds");
            }
        }

        public long TimeElapsed
        {
            get { return timeElapsed; }
            set
            {
                timeElapsed = value;
                OnPropertyChanged("TimeElapsed");
            }
        }

        public BitmapSource BitSource
        {
            get { return bitSource; }
            set
            {
                bitSource = value;
                OnPropertyChanged("BitSource");
            }
        }
        #endregion

        public MainViewModel(System.Windows.Shapes.Rectangle wpfRectangle)
        {
            reader = new Reader();
            rectangleView = wpfRectangle;
        }

        #region Video playback control

        //Reading video from file capture
        private void ReadFileCapture()
        {
            currentFrame = reader.NewFrameInBitmapSource();
            if (currentFrame != null)
            {
                FileCapturePosition = reader.GetFileCapturePosition();
                if (FileCapturePosition < croppedEnd)
                {


                    BitSource = currentFrame;


                }
                else
                    PausePlaying();
            }
            else PausePlaying();

        }

        //Timer and stopwatch initialization
        private void InitPlaying()
        {
            if (filename != null)
            {
                stopWatch = new ReaderStopWatch();

                timerIdle = new DispatcherTimer
                     (
                     TimeSpan.FromMilliseconds(1000 / reader.GetFrameRate()),
                     DispatcherPriority.Background,
                     (s, b) =>
                     {
                         StopWatchIdle();
                     },
                     Application.Current.Dispatcher
                     );

                timerIdle.Stop();
            }
        }

        //Play video
        private void StartPlaying()
        {
            if (stopWatch != null && timerIdle != null)
            {
                stopWatch.Start();
                timerIdle.Start();
            }
        }

        //New frame on stopwatch idle
        private void StopWatchIdle()
        {
            timeElapsed = stopWatch.ElapsedMilliseconds;
            timeElapsedInSeconds = Convert.ToInt64(stopWatch.Elapsed.TotalSeconds);
            long frameNumber = Convert.ToInt64(timeElapsed / (1000 / reader.GetFrameRate()));
            if (frameNumber <= croppedEnd)
            {
                FileCapturePosition = frameNumber;
            }
            else FileCapturePosition = croppedEnd;

            TimeText = TimeSpan.FromMilliseconds(timeElapsed).ToString(@"hh\:mm\:ss\:ff");
            reader.GetFrameOnPosition(fileCapturePosition - 1);
            ReadFileCapture();
            rectangleDecorator.SetRectangle(tracerViewModel.GetFrame((uint)fileCapturePosition));
        }

        private void PausePlaying()
        {
            if (timerIdle != null && stopWatch != null)
            {
                timerIdle.Stop();
                stopWatch.Stop();
            }
        }

        private void ResumePlaying()
        {
            if (timerIdle != null && stopWatch != null)
            {
                StartPlaying();
            }
            else if (filename == null)
            {
                OpenFile();
            }
            else InitPlaying();
        }

        private void StopPlaying()
        {
            if (timerIdle != null && stopWatch != null)
            {
                timerIdle.Stop();
                stopWatch.Stop();
                stopWatch.Reset();
                stopWatch.StartOffset = TimeSpan.FromMilliseconds(0);
                reader.SeekToStart();
                fileCapturePosition = reader.GetFileCapturePosition();
                Update();
            }
        }

        private void SeekToPosition(long Position)
        {
            PausePlaying();
            reader.GetFrameOnPosition(Position);
            rectangleDecorator.SetRectangle(tracerViewModel.GetFrame((uint)Position));
            //ResumePlaying();
        }

        private void ReadFirstFrame()
        {
            IsProjectLoaded = true;
            reader.GetFrameOnPosition(0);
            ReadFileCapture();
            rectangleDecorator.SetRectangle(tracerViewModel.GetFrame((uint)fileCapturePosition));
            InitPlaying();
        }

        private long GetCurrentPosition()
        {
            return reader.GetFileCapturePosition();
        }
        #endregion

        #region Click and key events

        //Handle video control actions
        private void HandleVideo(string action)
        {
            switch (action)
            {
                case "Play":
                    PlayButtonClick();
                    break;
                case "Pause":
                    PauseButtonClick();
                    break;
                case "Stop":
                    StopButtonClick();
                    break;
                case "Previous":
                    LeftButtonClick();
                    break;
                case "Next":
                    RightButtonClick();
                    break;
            }
        }

        //Handle app control actions
        private void HandleApp(string action)
        {
            switch (action)
            {
                case "Exit":
                    ExitClick();
                    break;
                case "Properties":
                    ChangePropertiesClick();
                    break;
                case "Analyze":
                    AnalyzeButtonClick();
                    break;
            }
        }

        //Handle project control actions
        private void HandleProject(string action)
        {
            switch (action)
            {
                case "Create":
                    CreateProject();
                    break;
                case "Load":
                    LoadProjectClick();
                    break;
                case "Save":
                    SaveProjectClick();
                    break;
            }
        }

        private void ExitClick()
        {
            reader.CloseSource();
            Application.Current.Shutdown();
        }

        private void PlayButtonClick()
        {
            ResumePlaying();
        }

        private void PauseButtonClick()
        {
            PausePlaying();
        }

        private void StopButtonClick()
        {
            StopPlaying();
        }

        private void SaveProjectClick()
        {
            try
            {
                if (tracerViewModel != null)
                {
                    string Directory = System.IO.Path.GetDirectoryName(this.filename);
                    string Timestamp = GetTimestamp(DateTime.Now);
                    string FileName = (System.IO.Path.GetFileNameWithoutExtension(this.filename) + "_" + Timestamp + ".trmn");
                    string PathOfProjectFile = System.IO.Path.Combine(Directory, FileName);
                    SaveProject(PathOfProjectFile);
                }
            }
            catch (Exception except)
            {
                MetroSimpleDialog("Error", except.Message);
            }
        }

        private async void CreateProject()
        {
            if (filename != null)
            {
                MessageDialogResult result = await ShowMessage("Close Confirmation", "Close current project ? Any unsaved data will be deleted!", MessageDialogStyle.AffirmativeAndNegative);

                switch (result)
                {
                    case MessageDialogResult.Affirmative: OpenFile(); break;
                    case MessageDialogResult.Negative: break;
                    default: break;
                }
            }
            else OpenFile();
        }

        private async void LoadProjectClick()
        {
            try
            {
                if (filename != null)
                {
                    MessageDialogResult result = await ShowMessage("Close Confirmation", "Close current project ? Any unsaved data will be deleted!", MessageDialogStyle.AffirmativeAndNegative);

                    switch (result)
                    {
                        case MessageDialogResult.Affirmative: LoadProject(); break;
                        case MessageDialogResult.Negative: break;
                        default: break;
                    }
                }
                else LoadProject();
            }
            catch (Exception except)
            {
                MetroSimpleDialog("Error", except.Message);
            }
        }

        private void ChangePropertiesClick()
        {
            propertiesView = PropertiesView.GetInstance();
            propertiesView.Show();
            propertiesView.Activate();
        }

        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && rectangleDecorator != null)
            {
                rectangleDecorator.SaveAOIRectanglePosition();
            }
        }

        private void AnalyzeButtonClick()
        {
            Thread analyzeThread = new Thread(Analyze);
            chartViewModel = new ChartViewModel(new Chart());
            chartView = ChartView.GetInstance(chartViewModel);
            windowLength = Properties.Settings.Default.LengthOfFrameWindow;
            TotalFramesToAnalyze = (fileCaptureLength + 1 - windowLength) - CroppedStart;
            if (croppedEnd <= TotalFramesToAnalyze) TotalFramesToAnalyze = croppedEnd;
            analyzerPool = new Analyzer[fileCaptureLength + 1];
            analyzeThread.Start();
            chartView.Show();
        }

        private void LeftButtonClick()
        {
            FileCapturePosition -= 1;
            Update();
        }

        private void RightButtonClick()
        {
            FileCapturePosition += 1;
            Update();
        }
        #endregion

        #region Project control

        //Save project in XML format
        private void SaveProject(string PathOfProjectFile)
        {
            XElement root = new XElement("Project", new XElement("PathToVideoSource", System.IO.Path.GetFileName(this.filename)));
            root.Add(tracerViewModel.AreasOfInterestToXML());
            root.Save(PathOfProjectFile);

            MetroSimpleDialog("Saved", "Project saved to: " + PathOfProjectFile);
        }

        //Load project from xml
        private void LoadProject()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "TremAn project|*.trmn|All Files|*.*";
            if (openFileDialog.ShowDialog() == true)
            {
                string PathOfProjectFile = openFileDialog.FileName;
                XElement AOIRectangles = XElement.Load(PathOfProjectFile);
                tracer = TracerViewModel.FromXML(AOIRectangles);
                string VideoSourceName = AOIRectangles.Descendants().Where(n => n.Name == "PathToVideoSource").FirstOrDefault().Value;
                string Directory = System.IO.Path.GetDirectoryName(PathOfProjectFile);
                this.filename = System.IO.Path.Combine(Directory, VideoSourceName);
                BuildProject();
                ReadFirstFrame();
                return;
            }
            else return;
        }

        //Build new project
        private void BuildProject()
        {
            reader.Initialize(filename);
            if (reader.GetFileCaptureLength() > uint.MaxValue)
            {
                filename = null;
                throw new Exception("Too many video frames");
            }

            if (reader.GetFileCaptureLength() <= Properties.Settings.Default.LengthOfFrameWindow)
            {
                filename = null;
                throw new Exception("Video is too short. Minimal length is " + Properties.Settings.Default.LengthOfFrameWindow + " video frames");
            }

            FileCaptureLength = reader.GetFileCaptureLength();
            CroppedEnd = fileCaptureLength;
            analyzerPool = null;
            chartViewModel = null;
            AnalyzationProgress = 0;
            if (tracer == null)
                tracer = new Tracer((uint)fileCaptureLength + 1);
            tracerViewModel = new TracerViewModel(tracer);
            rectangleDecorator = new RectangleDecorator(tracerViewModel.GetFrame(0), rectangleView);
            tracerViewModel.SetAOIRectCtrlCallback(rectangleDecorator);

            TotalTimeInSeconds = Convert.ToInt64(fileCaptureLength / reader.GetFrameRate());
        }

        //Open video file
        private void OpenFile()
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "Media Files|*.mp4;*.mpg;*.avi;*.wma;*.wmv;*.mov;*.mkv;|All Files|*.*";
                if (openFileDialog.ShowDialog() == true)
                {
                    filename = openFileDialog.FileName;
                    if (tracer != null) tracer = null;
                    BuildProject();
                    ReadFirstFrame();
                    return;
                }
                else return;
            }
            catch (BadImageFormatException)
            {
                MetroSimpleDialog("Error", "Bad Image Format");
                filename = null;
            }
            catch (Exception Exception)
            {
                MetroSimpleDialog("Error", Exception.Message);
                filename = null;
            }
        }

        private String GetTimestamp(DateTime value)
        {
            return value.ToString("yyyy_MM_dd_HHmmssfff");
        }
        #endregion

        #region Analyzation methods

        //Analyze video
        private void Analyze()
        {
            try
            {
                double[] frequencyArray = Analyzer.GenerateFrequencyVector(Properties.Settings.Default.LowestFrequency, windowLength, reader.GetFrameRate());
                PointOfInterest[] pointsGrid = Analyzer.GenerateGrid(reader.GetWidth(), reader.GetHeight(), Properties.Settings.Default.GridStep);
                List<Bgr<byte>[,]> startupFrameBuffer = new List<Bgr<byte>[,]>();
                Reader analyzationVideoReader = new Reader();
                analyzationVideoReader.Initialize(filename);
                analyzationVideoReader.StartingPosition = croppedStart;
                analyzationVideoReader.SeekToStart();
                int threadMax = Properties.Settings.Default.ThreadMax;

                for (int j = 0; j < windowLength + threadMax; j++)
                {
                    Bgr<byte>[,] positionOfAnalyzation = analyzationVideoReader.NewFrame();
                    startupFrameBuffer.Add(positionOfAnalyzation);
                }

                for (uint i = 0; i < totalFramesToAnalyze; i += Convert.ToUInt32(threadMax))
                {

                    var doneEvents = new CountdownEvent(1);
                    analyzationVideoReader.GetFrameOnPosition(i + windowLength);
                    AnalyzationProgress = i;

                    if (i > 0)
                    {
                        startupFrameBuffer.RemoveRange(0, threadMax);
                        startupFrameBuffer.AddRange(GetNextFrames(threadMax, analyzationVideoReader));
                    }

                    for (int j = 0; j < threadMax; j++)
                    {
                        doneEvents.AddCount();
                        Rectangle currentAreaOfInterest = tracerViewModel.GetFrame(i + Convert.ToUInt32(j));
                        Analyzer analyzer = new Analyzer(analyzationVideoReader.GetFrameRate(), windowLength);

                        analyzer.SetGrid(pointsGrid);
                        analyzer.SetFrequencyArray(frequencyArray);
                        analyzerPool[croppedStart + i + j] = analyzer;

                        ThreadPool.QueueUserWorkItem(
                            (state) =>
                            {
                                try
                                {
                                    analyzer.StartProcessing(currentAreaOfInterest, startupFrameBuffer, j);
                                }
                                finally
                                {
                                    doneEvents.Signal();
                                }
                            });
                    }
                    doneEvents.Signal();
                    doneEvents.Wait();
                }

                analyzationVideoReader.CloseSource();
                chartViewModel.FreqProgressChartModel = CreateProgressPlotModel();
                if (analyzerPool[fileCapturePosition] != null)
                {
                    chartViewModel.FreqAnalysisChartModel = analyzerPool[fileCapturePosition].FrequencyAnalysisPlotModel;
                    chartViewModel.CurrentFrequency = analyzerPool[fileCapturePosition].MaxFrequency;
                }
                CreateFreqAnalysisPlotModels();
                GenerateDataToExport();
            }
            catch (FileNotFoundException)
            {
                MetroSimpleDialog("Warning", "There is no video to analyze. Please import one or load TremAn project!");
            }
        }

        //Get next buffer of frames from video source
        private List<Bgr<byte>[,]> GetNextFrames(int numberOfFrames, Reader analyzationVideoReader)
        {
            List<Bgr<byte>[,]> nextFrames = new List<Bgr<byte>[,]>();

            for (int i = 0; i < numberOfFrames; i++)
            {
                nextFrames.Add(analyzationVideoReader.NewFrame());
            }
            return nextFrames;
        }
        #endregion

        # region Creating plot model and generating data to export
        private PlotModel CreateProgressPlotModel()
        {
            LineSeries series = new LineSeries();
            for (int i = 0; i < analyzerPool.Count(); i++)
            {
                if (analyzerPool[i] != null)
                    series.Points.Add(new DataPoint(i, analyzerPool[i].MaxFrequency));
            }
            PlotModel model = new PlotModel { Title = "Frequency progress" };
            model.PlotType = PlotType.XY;
            var frequencyAxis = new LinearAxis()
            {
                Position = AxisPosition.Left,
                Title = "Frequency(Hz)"
            };
            model.Series.Add(series);
            model.Axes.Add(frequencyAxis);
            return model;
        }

        private void CreateFreqAnalysisPlotModels()
        {
            chartViewModel.FreqAnalysisChartModels = new PlotModel[analyzerPool.Count()];
            for (int i = 0; i < analyzerPool.Count(); i++)
            {
                if (analyzerPool[i] != null)
                    chartViewModel.FreqAnalysisChartModels[i] = analyzerPool[i].FrequencyAnalysisPlotModel;
            }
        }

        private void GenerateDataToExport()
        {
            var spectrs = (from analyzer in analyzerPool
                               where analyzer != null
                               select analyzer.ResultedSpectrum).ToArray();
            var frequencies = (from analyzer in analyzerPool
                               where analyzer != null
                               select analyzer.MaxFrequency).ToArray();
            chartViewModel.SpectrumToExport = spectrs;
            chartViewModel.FrequenciesToExport = frequencies;
        }
        #endregion

        #region Other methods
        //Invoke on property change
        private void OnPropertyChanged(String info)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(info));
        }

        //Update variables
        private void Update()
        {
            offsetTimeElapsed = Convert.ToInt64(fileCapturePosition * (1000 / reader.GetFrameRate()));
            TimeSpan offsetTimeSpan = TimeSpan.FromMilliseconds(offsetTimeElapsed);
            stopWatch.Stop();
            reader.GetFrameOnPosition(fileCapturePosition);
            stopWatch = new ReaderStopWatch(offsetTimeSpan);
            if (!stopWatch.IsRunning)
            {
                StopWatchIdle();
            }
            else stopWatch.Start();
        }

        private void Slider_TargetUpdated(double barValue)
        {
            if (barValue >= croppedStart && barValue <= croppedEnd)
                Update();
        }

        //Save position of rectangle in video
        private void TracerSavePosition()
        {
            if (tracerViewModel != null)
            {
                tracerViewModel.SaveAOIPosition(rectangleDecorator.GetAreaOfInterestRectangle());
            }
        }

        //Closing the video file source on MainView close
        public void OnWindowClosing(object sender, CancelEventArgs e)
        {
            reader.CloseSource();
        }
        #endregion

        #region Dialogs
        private async void MetroSimpleDialog(string Title, string Content)
        {
            var MainMetroWindow = (Application.Current.MainWindow as MetroWindow);
            MainMetroWindow.MetroDialogOptions.ColorScheme = MetroDialogColorScheme.Accented;

            await MainMetroWindow.ShowMessageAsync(Title, Content, MessageDialogStyle.Affirmative, MainMetroWindow.MetroDialogOptions);
        }

        private async Task<MessageDialogResult> ShowMessage(string Title, string Content, MessageDialogStyle dialogStyle)
        {
            var MainMetroWindow = (Application.Current.MainWindow as MetroWindow);
            MainMetroWindow.MetroDialogOptions.ColorScheme = MetroDialogColorScheme.Accented;

            return await MainMetroWindow.ShowMessageAsync(Title, Content, dialogStyle, MainMetroWindow.MetroDialogOptions);
        }
        #endregion
    }
}
