﻿using System;
using System.ComponentModel;
using System.Windows.Input;

namespace TremAn.ViewModels
{
    public class PropertiesViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private ICommand propertiesCommand;


        public ICommand PropertiesCommand
        {
            get
            {
                if (propertiesCommand == null)
                {
                    propertiesCommand = new Command(p => true, p => this.PropertiesHandler((string)p));
                }
                return propertiesCommand;
            }
        }

        private void Reset()
        {
            Properties.Settings.Default.Reset();
        }

        private void Save()
        {
            Properties.Settings.Default.Save();
        }

        private void ComboBox_SelectionChanged()
        {
            Properties.Settings.Default.Save();
        }

        private void PropertiesHandler(string parameter)
        {
            switch (parameter)
            {
                case "SAVE":
                    Save();
                    break;
                case "RESET":
                    Reset();
                    break;
            }
        }

        private void OnPropertyChanged(String info)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(info));
        }
    }
}
