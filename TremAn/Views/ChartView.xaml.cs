﻿using System.Windows;
using System.ComponentModel;
using TremAn.ViewModels;
using MahApps.Metro.Controls;

namespace TremAn.Views
{
    /// <summary>
    /// Interaction logic for ChartView.xaml
    /// </summary>
    public sealed partial class ChartView : MetroWindow
    {
        private static ChartView instance;
        private static object @lock = new object();

        private ChartView(ChartViewModel chartViewModel)
        {
            InitializeComponent();
            this.DataContext = chartViewModel;
        }

        public static ChartView GetInstance(ChartViewModel chartViewModel)
        {
            lock (@lock)
            {
                if (instance == null) instance = new ChartView(chartViewModel);
            else instance.DataContext = chartViewModel;
            return instance;
            }
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
            this.Visibility = Visibility.Hidden;
        }
    }
}
