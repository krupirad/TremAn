﻿using TremAn.ViewModels;
using MahApps.Metro.Controls;

namespace TremAn.Views
{
    /// <summary>
    /// Interaction logic for 
    /// 
    //MainView.xaml
    /// </summary>
    public partial class MainView : MetroWindow
    {   
        public MainView()
        {
            InitializeComponent();
            MainViewModel mainViewModel = new MainViewModel(rectangleView);
            DataContext = mainViewModel;
            Closing += mainViewModel.OnWindowClosing;
        }

    }
}
