﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using TremAn.ViewModels;

namespace TremAn.Views
{
    /// <summary>
    /// Interaction logic for PropertiesView.xaml
    /// </summary>
    public sealed partial class PropertiesView : MetroWindow
    {
        private static PropertiesView instance;
        private static object @lock = new object();

        private PropertiesView()
        {
            InitializeComponent();
            this.DataContext = new PropertiesViewModel();
        }

        public static PropertiesView GetInstance()
        {
            lock (@lock)
            {
                if (instance == null) instance = new PropertiesView();
                return instance;
            }
        }
       
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            this.Visibility = Visibility.Hidden;
        }
    }
}
